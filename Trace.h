#ifdef TRACE_H
#define TRACE_H

#if defined(DEBUG)
  #define TRACE(...) \
    do { printf(__VA_ARGS__); } while (0)
#else
  #define TRACE(...) \
    do { if (0) printf(__VA_ARGS__); } while (0)
#endif

#endif

